from pathlib import Path
from utils import read_and_backup
import re

_test_data = Path(__file__).parent / "data"


def test_micropip(selenium_py3):
    # at this point, folium should have loaded requests
    # so we check that this is our version
    result = selenium_py3.run_basthon("""
    import requests
    requests.__author__ == 'Romain Casati'""")
    assert result['stdout'] == ""
    assert result['stderr'] == ""
    assert result['result']['result']['text/plain'] == 'True'


def test_scipy(selenium_py3):
    result = selenium_py3.run_basthon("""
from scipy.integrate import solve_ivp
import numpy as np

def f(t, y, a):
    return [-a * y[1], a * y[0]]

y0 = [1, 0]
sol = solve_ivp(f, (0, np.pi), y0, args=(2,), atol=1e-13, rtol=1e-10)
y = sol.y
np.allclose(y[:, -1], y0)""")
    assert result['stdout'] == ""
    assert result['stderr'] == ""
    assert result['result']['result']['text/plain'] == 'True'


def test_lolviz(selenium_py3):
    result = selenium_py3.run_basthon("""
import lolviz

ma_liste = ['hi', 'mom', {3, 4}, {"parrt":"user"}]

o = lolviz.listviz(ma_liste)
print(o.source)
o
""")
    assert result['stderr'] == ""
    gv = re.sub('node[0-9]+', 'node', result['stdout'])
    target = read_and_backup(_test_data / "python3_lolviz_source.gv", gv)
    assert gv == target
    svg = re.sub('node[0-9]+', 'node',
                 result['result']['result']['image/svg+xml'])
    target = read_and_backup(_test_data / "python3_lolviz.svg", svg)
    assert svg == target


def test_binarytree(selenium_py3):
    from ast import literal_eval
    result = selenium_py3.run_basthon("""
from binarytree import Node

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print(root)
root.graphviz().source
""")
    assert result['stderr'] == ""
    assert result['stdout'] == """
    __1
   /   \\
  2     3
 / \\
4   5

"""
    gv = literal_eval(result['result']['result']['text/plain'])
    gv = re.sub('[0-9]+:l', ':l', gv)
    gv = re.sub('[0-9]+:r', ':r', gv)
    gv = re.sub('[0-9]+:v', ':v', gv)
    gv = re.sub(r'[0-9]+ -> [0-9]+ \[label=', ' ->  [label=', gv)
    gv = re.sub(r'[0-9]+ \[label=', ' [label=', gv)
    target = read_and_backup(_test_data / "python3_binarytree.gv", gv)
    assert gv == target


def test_audioop(selenium_py3):
    result = selenium_py3.run_basthon("import wave")
    assert result['stdout'] == ""
    assert result['stderr'] == ""


def test_doctest(selenium_py3):
    # this ensure our __main__ is properly connected to globals()
    # since doctest.testmod will look in __main__
    selenium_py3.run_basthon("""
import doctest

def my_test(x):
    '''
    >>> my_test(2)
    4
    >>> my_test(4)
    15
    '''
    return x ** 2
""")
    data = selenium_py3.run_basthon("str(doctest.testmod(verbose=False))")
    assert data['stderr'] == ""
    assert data['result']['result']['text/plain'] == "'TestResults(failed=1, attempted=2)'"


def test_completion(selenium_py3):
    assert selenium_py3.run_js("return Basthon.complete('print.__d')") == [[
        'print.__delattr__(',
        'print.__dir__()',
        'print.__doc__'], 0]
