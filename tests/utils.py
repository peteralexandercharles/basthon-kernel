import os


def read_and_backup(path, data, binary=False):
    """
    Return the file content at `path` after backuping data at the same 
    Destination but with '.generated_during_tests' suffix (before extension).
    """
    r, w = 'r', 'w'
    if binary:
        r += 'b'
        w += 'b'

    path_generated = os.path.splitext(path)
    path_generated = path_generated[0] + '.generated_during_tests' + path_generated[1]
    with open(path_generated, w) as f:
        f.write(data)

    with open(path, r) as f:
        return f.read()
