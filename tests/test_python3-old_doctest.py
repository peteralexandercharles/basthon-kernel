def test_globals(selenium_py3old):
    # this ensure our __main__ is properly connected to globals()
    # since doctest.testmod will look in __main__
    selenium_py3old.run_basthon("""
import doctest

def my_test(x):
    '''
    >>> my_test(2)
    4
    >>> my_test(4)
    15
    '''
    return x ** 2
""")
    data = selenium_py3old.run_basthon("str(doctest.testmod(verbose=False))")
    assert data['stderr'] == ""
    assert data['result']['result']['text/plain'] == "'TestResults(failed=1, attempted=2)'"
