from pathlib import Path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from utils import read_and_backup


def test_patch_input(selenium_py3old):
    selenium_py3old.run_basthon_detach("int(input('How old are you?'))")
    driver = selenium_py3old.driver
    wait = WebDriverWait(driver, 10)
    wait.until(expected_conditions.alert_is_present())
    alert = driver.switch_to.alert
    alert.send_keys("42")
    alert.accept()
    data = selenium_py3old.run_basthon_reattach()
    assert data['stderr'] == ""
    result = data['result']['result']['text/plain']
    assert result == "42"


def test_patch_modules(selenium_py3old):
    data = selenium_py3old.run_basthon("help('modules')")
    assert 'result' not in data['result'] and data['stderr'] == ""
    text = data['stdout']
    target = read_and_backup(Path(__file__).parent / 'data' / 'python3-old_modules.txt', text)
    assert text == target


def test_patch_six(selenium_py3old):
    data = selenium_py3old.run_basthon("""
    from pathlib import Path

    Path('/lib/python3.8/six.py').exists() and not Path('/lib/python3.8/site-packages/six.py').exists()""")
    assert data['stdout'] == ""
    assert data['stderr'] == ""
    assert data['result']['result']['text/plain'] == 'True'
