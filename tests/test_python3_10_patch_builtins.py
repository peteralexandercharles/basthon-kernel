from pathlib import Path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from utils import read_and_backup


def test_patch_input(selenium_py3):
    data = selenium_py3.run_basthon("input('How old are you?')")
    assert data['stderr'] == ""
    assert data['stdout'] == ""
    result = data['result']['result']['text/plain']
    assert result == "'Hello Basthon!'"

    selenium_py3.run_basthon_detach("""
def f(x):
    return input(x)
int(f("How old are you?"))
""")
    driver = selenium_py3.driver
    wait = WebDriverWait(driver, 10)
    wait.until(expected_conditions.alert_is_present())
    alert = driver.switch_to.alert
    alert.send_keys("42")
    alert.accept()
    data = selenium_py3.run_basthon_reattach()
    assert data['stderr'] == ""
    result = data['result']['result']['text/plain']
    assert result == "42"


def test_patch_modules(selenium_py3):
    # importing pkg_resources (setuptools) before using help to avoid warning
    # 'Distutils was imported before Setuptools'
    # TODO: find a way to import setuptools before using help
    #       (async plays against us here)
    # see also test_pkg_resources
    data = selenium_py3.run_basthon("import pkg_resources ; help('modules')")
    assert 'result' not in data['result']
    assert data['stderr'] == ""
    text = data['stdout']
    target = read_and_backup(Path(__file__).parent / 'data' / 'python3_modules.txt', text)
    assert text == target
