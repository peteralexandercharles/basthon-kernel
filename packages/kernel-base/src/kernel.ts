import { PromiseDelegate } from "promise-delegate";
import { VERSION } from './version';

/**
 * An error thrown by not implemented API functions.
 */
class NotImplementedError extends Error {
    constructor(funcName: string) {
        super(`Function ${funcName} not implemented!`);
        this.name = "NotImplementedError";
    }
}

/**
 * Event for Basthon's dispatch/listen.
 */
class BasthonEvent extends Event {
    public detail: any;
    constructor(id: string, data: any) {
        super(id);
        this.detail = data;
    }
}

/**
 * API that any Basthon kernel should fill to be supported
 * in console/notebook.
 */
export class KernelBase {

    private _rootPath: string;
    private _ready: boolean = false;
    private readonly _loaded = new PromiseDelegate<void>();
    public _execution_count: number = 0;

    constructor(rootPath: string) {
        // root path where kernel is installed
        this._rootPath = rootPath;
    }

    /**
     * Kernel version number (string).
     */
    public version(): string { return VERSION; };

    /**
     * Language implemented in the kernel (string).
     * Generally lower case.
     */
    public language(): string { throw new NotImplementedError("language"); };

    /**
     * Language name implemented in the kernel (string).
     * As it should be displayed in text.
     */
    public languageName(): string { throw new NotImplementedError("languageName"); };

    /**
     * Script (module) file extensions
     */
    public moduleExts(): string[] {
        throw new NotImplementedError("moduleExts");
    };

    /**
     * Launch the kernel.
     */
    public async launch() { throw new NotImplementedError("launch"); };

    /**
     * Execution count getter.
     */
    public get execution_count() { return this._execution_count; }

    /**
     * Async code evaluation that resolves with the result.
     */
    public evalAsync(
        code: string,
        outCallback: (_: string) => void,
        errCallback: (_: string) => void,
        data: any = null): Promise<any> {
        throw new NotImplementedError("evalAsync");
    };

    public restart(): void { throw new NotImplementedError("restart"); };

    public putFile(filename: string, content: ArrayBuffer): void {
        throw new NotImplementedError("putFile");
    };

    public putModule(filename: string, content: ArrayBuffer): void {
        throw new NotImplementedError("putModule");
    };

    public userModules(): string[] { return []; };

    public getFile(path: string) { throw new NotImplementedError("getFile"); };

    public getUserModuleFile(filename: string) {
        throw new NotImplementedError("getUserModuleFile");
    };

    public more(source: string): boolean { throw new NotImplementedError("more"); };

    public complete(code: string): [string[], number] | [] { return []; };

    public banner(): string { return `Welcome to the ${this.languageName()} REPL!`; };

    public ps1(): string { return ">>> "; };

    public ps2(): string { return "... "; };

    /**
     * Initialize the kernel.
     */
    public async init() {
        try {
            await this.launch();
        } catch (error) {
            this._loaded.reject(error);
            return;
        }
        // connecting eval to basthon.eval.request event.
        this.addEventListener("eval.request", this.evalFromEvent.bind(this));
        this._ready = true;
        this._loaded.resolve();
    }

    /**
     * Is the kernel ready?
     */
    public get ready() { return this._ready; }

    /**
     * Promise that resolve when the kernel is loaded.
     */
    public async loaded() { await this._loaded.promise; }

    /**
     * Root for kernel files. This is always the language directory
     * inside the version number directory inside the kernel directory.
     */
    public basthonRoot(absolute: boolean = false): string {
        let url = this._rootPath + "/" + this.version() + "/" + this.language();
        if (absolute && !url.startsWith("http")) {
            const base = window.location.origin + window.location.pathname;
            url = base.substring(0, base.lastIndexOf('/')) + "/" + url;
        }
        return url;
    }

    /**
     * Downloading data (bytes array or data URL) as filename
     * (opening browser dialog).
     */
    public download(data: Uint8Array | string, filename: string): void {
        if (!(typeof data === 'string' || data instanceof String)) {
            const blob = new Blob([data], { type: "application/octet-stream" });
            data = window.URL.createObjectURL(blob);
        }
        const anchor = document.createElement("a");
        anchor.download = filename;
        anchor.href = data as string;
        anchor.target = "_blank";
        anchor.style.display = "none"; // just to be safe!
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    };

    /**
     * Dynamically load a script asynchronously.
     */
    public static loadScript(url: string) {
        return new Promise<any>(function(resolve, reject) {
            let script = document.createElement('script');
            script.onload = resolve;
            script.onerror = reject;
            script.src = url;
            document.head.appendChild(script);
        });
    };

    /**
     * Wrapper around document.dispatchEvent.
     * It adds the 'basthon.' prefix to each event name and
     * manage the event lookup to retreive relevent data.
     */
    public dispatchEvent(eventName: string, data: any): void {
        document.dispatchEvent(new BasthonEvent(`basthon.${eventName}`, data));
    };

    /**
     * Wrapper around document.addEventListener.
     * It manages the 'basthon.' prefix to each event name and
     * manage the event lookup to retreive relevent data.
     */
    public addEventListener(eventName: string, callback: (_: any) => void): void {
        document.addEventListener(
            `basthon.${eventName}` as keyof DocumentEventMap,
            function(event: Event) { callback((event as BasthonEvent).detail); });
    };

    /**
     * Send eval.input event then wait for the user response and return it.
     */
    public async inputAsync(
        prompt: string | null | undefined,
        password: boolean = false,
        data: any = undefined) {
        data = this.clone(data);
        data.content = { prompt, password };
        const promise = new Promise(function(resolve, reject) {
            data.resolve = resolve;
            data.reject = reject;
        });
        this.dispatchEvent("eval.input", data);
        return await promise;
    }

    /**
     * Simple clone via JSON copy.
     */
    public clone(obj: any): any {
        // simple trick that is enough for our purpose.
        return JSON.parse(JSON.stringify(obj));
    };

    /**
     * Put a ressource (file or module).
     * Detection is based on extension.
     */
    public putRessource(filename: string, content: ArrayBuffer) {
        const ext = filename.split('.').pop() ?? "";
        if (this.moduleExts().includes(ext)) {
            return this.putModule(filename, content);
        } else {
            return this.putFile(filename, content);
        }
    };

    /**
     * Internal. Code evaluation after an eval.request event.
     */
    public async evalFromEvent(data: any) {
        const stdCallback = (std: string) =>
            (text: string) => {
                let dataEvent = this.clone(data);
                dataEvent.stream = std;
                dataEvent.content = text;
                this.dispatchEvent("eval.output", dataEvent);
            };
        const outCallback = stdCallback("stdout");
        const errCallback = stdCallback("stderr");

        let args;
        try {
            args = await this.evalAsync(data.code, outCallback, errCallback, data);
        } catch (error: any) {
            errCallback(error.toString());
            const dataEvent = this.clone(data);
            dataEvent.error = error;
            dataEvent.execution_count = this.execution_count;
            this.dispatchEvent("eval.error", dataEvent);
            return;
        }
        if (args == null) return;  // this should not happend
        const result = args[0];
        const executionCount = args[1];
        let dataEvent = this.clone(data);
        dataEvent.execution_count = executionCount;
        if (result != null) dataEvent.result = result;
        this.dispatchEvent("eval.finished", dataEvent);
    }
}
