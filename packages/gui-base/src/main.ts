import { PromiseDelegate } from "promise-delegate";
import { CheckpointsManager } from "@basthon/checkpoints";
import { KernelBase } from "@basthon/kernel-base";
import { KernelLoader } from "@basthon/kernel-loader";

declare global {
    interface Window {
        // just in case we want to force empty content at load
        _basthonEmptyContent?: any;
    }
}

export interface GUIOptions {
    kernelRootPath: string,
    language: string,
    uiName?: string,
    noCheckpointsInit?: boolean,
}


/**
 * Base class for console and notebook GUI.
 */
export class GUIBase {
    private readonly _language: string;
    private readonly _loaded = new PromiseDelegate<void>();
    private _loader: KernelLoader;
    private _checkpoints?: CheckpointsManager<string>;
    private _maxCheckpoints = 10;
    protected _contentFilename: string = "content.txt";
    protected _urlKey: string = "content";

    /* console errors redirected to notification system */
    private _console_error: (message: string) => void = console.error;

    public constructor(options: GUIOptions) {
        this._language = options.language;
        this._loader = new KernelLoader(options.kernelRootPath, options.language);
        // loading Basthon (errors are fatal)
        this._loader.showLoader("Chargement de Basthon...", false, false);
        /* per language checkpoints */
        if (!options.noCheckpointsInit)
            this._checkpoints = new CheckpointsManager<string>(
                `${options.uiName}.${options.language}`, this._maxCheckpoints);
    }

    /**
     * Language getter.
     */
    public get language() { return this._language; }

    /**
     * Kernel getter.
     */
    public get kernel() { return this._loader.kernel; }
    public get kernelSafe() { return this._loader.kernelSafe; }

    /**
     * KernelLoader getter.
     */
    public get kernelLoader() { return this._loader; }

    /**
     * Check if GUI is loaded.
     */
    public async loaded() { await this._loaded.promise; }

    /**
     * Wait for par load (document.body available).
     */
    public async pageLoad() { await this._loader.pageLoad(); }

    /**
     * Set the checkpoints manager
     * (typically use with GUIOptions.noCheckpointsInit set to true).
     */
    public setCheckpointsManager(checkpoints: CheckpointsManager<string>) {
        this._checkpoints = checkpoints;
    }

    /**
     * Notify the user.
     */
    public info(title: string, message: string) { }

    /**
     * Notify the user with an error.
     */
    public error(title: string, message: string) { }

    /**
     * Ask the user to confirm or cancel.
     */
    public confirm(
        title: string,
        message: string,
        text: string,
        callback: (() => void),
        textCancel: string,
        callbackCancel: (() => void)): void { }

    /**
     * Ask the user to select a choice.
     */
    public select(
        title: string,
        message: string,
        choices: {
            text: string,
            handler: () => void
        }[],
        textCancel: string,
        callbackCancel: (() => void)) { }

    /**
     * The error notification system.
     */
    public notifyError(error: Error | ErrorEvent | PromiseRejectionEvent) {
        this._console_error(error as unknown as string);
        let message = (error as Error).message;
        if (message == null)
            message = (error as PromiseRejectionEvent)?.reason?.message;
        if (message == null)
            message = error.toString();
        message = message.split('\n').join('<br>');
        this.error("Erreur", `Erreur : ${message}`);
        // In case of error, force loader hiding.
        try {
            this._loader.hideLoader();
        } catch (e) { }
    }

    /**
     * Initialize the GUI.
     */
    public async init(options: any = null) {
        try {
            await this._init(options);
            this._loaded.resolve();
        } catch (error) {
            this._loaded.reject(error);
        }
    }

    /**
     * Get the content (script or notebook content).
     */
    public content(): string { return ""; }

    /**
     * Set the content (script or notebook content).
     */
    public setContent(content: string): void { }

    /**
     * Loading the content from query string (ipynb=/script= or from=).
     */
    public async loadFromQS(key: string): Promise<string | null> {
        const url = new URL(window.location.href);
        const from_key = 'from';
        let content: string | null = null;
        if (url.searchParams.has(key)) {
            content = url.searchParams.get(key) || "";
            try {
                content = await this.inflate(content);
            } catch (error) {
                /* backward compatibility with non compressed param */
                if (content != null) content = decodeURIComponent(content);
            }
        } else if (url.searchParams.has(from_key)) {
            let fileURL = url.searchParams.get(from_key);
            if (fileURL != null) fileURL = decodeURIComponent(fileURL);
            try {
                const response = await fetch(fileURL as string);
                if (!response.ok) throw new Error(response.statusText);
                content = await response.text();
            } catch (error: any) {
                throw new Error(`Le chargement de ${fileURL} a échoué : ${error?.message ?? error.toString()}`);
            }
        }
        if (content != null) this.setContent(content);
        return content;
    }

    /**
     * Setup the UI (typically connect events etc..).
     */
    protected async setupUI(options: any): Promise<void> {
        this.kernelLoader.kernelAvailable().then((kernel: KernelBase) => {
            // this is ugly but it seems complicated to make
            // GUI available in websocket for the notebook
            // so patch it!
            const dispatchEvent = kernel.dispatchEvent.bind(this.kernel);
            kernel.dispatchEvent = async (event: string, data: any) => {
                if (event === "eval.request") await this.backup(false);
                dispatchEvent(event, data);
            }
            kernel.addEventListener("eval.finished", () => {
                this.validateBackup();
            });
            kernel.addEventListener("eval.error", () => {
                this.validateBackup();
            });
        });
    }

    /**
     * Load content at startup.
     */
    protected async loadInitialContent(options: any): Promise<void> {
        if (window._basthonEmptyContent) return;
        if (await this.loadFromQS(this._urlKey) != null) return;
        this.loadFromStorage();
    }

    /**
     * Load content from local forage.
     */
    public async loadFromStorage(setContent = true) {
        const approved = await this.lastBackupValid();
        let content: string | null = null;
        if (approved == null) {
            content = null;
        } else if (approved) {
            content = await this._checkpoints?.getLast() as string;
        } else {
            const promise = new PromiseDelegate<string | null>();
            this.confirm(
                "Récupération",
                "Il semble que Basthon ait rencontré un problème à sa dernière utilisation. Que voulez-vous faire ?",
                "Choisir une sauvegarde",
                async () => {
                    promise.resolve(await this.selectCheckpoint());
                },
                "Annuler",
                () => { promise.resolve(null); }
            );
            content = await promise.promise;
        }
        if (setContent && content != null) this.setContent(content);
        return content;
    }

    /**
     * Tag last backup as "approved".
     */
    public async validateBackup() {
        await this._checkpoints?.tagLast("approved");
    }

    /**
     * Is last backup tagged as "approved"?
     */
    public async lastBackupValid() {
        return await this._checkpoints?.lastHasTag("approved");
    }

    /**
     * Select a checkpoint to load in the script.
     */
    public async selectCheckpoint(): Promise<string | null> {
        const times = await this._checkpoints?.times();
        const promise = new PromiseDelegate<string | null>();
        const choices = times?.map(
            (t: string, i: number) => ({
                text: CheckpointsManager.toHumanDate(parseInt(t, 10)),
                handler: async () => { promise.resolve((await this._checkpoints?.getIndex(i)) ?? null); }
            }));
        if (choices == null) return null;
        if (choices.length === 0) {
            this.info("Aucune sauvegarde à restaurer",
                "Il n'y a aucune sauvegarde à restaurer !");
            promise.resolve(null);
        } else {
            this.select(
                "Choisissez une sauvegarde",
                "De quelle sauvegarde souhaitez-vous reprendre ?",
                choices,
                "Annuler",
                () => { promise.resolve(null); }
            );
        }
        return await promise.promise;
    }

    /**
     * Backup to checkpoints.
     */
    public async backup(approved = true) {
        return await this._checkpoints?.push(this.content(), approved ? ["approved"] : []);
    }

    /**
     * Internal GUI init.
     * It calls setupUI then loadInitialContent.
     */
    protected async _init(options: any = null) {
        /* all errors redirected to notification system */
        const onerror = this.notifyError.bind(this);
        window.addEventListener('error', onerror);
        window.addEventListener("unhandledrejection", onerror);
        console.error = (message) => onerror(new Error(message));

        await this._checkpoints?.ready();

        await this.setupUI(options);

        await this.loadInitialContent(options);

        await this.kernelLoader.kernelLoaded();

        const init = this.initCaller.bind(this);
        // loading aux files from URL
        await init(this.loadURLAux.bind(this), "Chargement des fichiers auxiliaires...", true);
        // loading modules from URL
        await init(this.loadURLModules.bind(this), "Chargement des modules annexes...", true);
        // end
        this.kernelLoader.hideLoader();

    }

    /**
     * Change loader text and call init function.
     * If catchError is false, in case of error, we continue the
     * init process, trying to do our best...
     */
    public async initCaller(func: () => Promise<any>, message: string, catchError: boolean) {
        this._loader.setLoaderText(message);
        try {
            return await func();
        } catch (error) {
            if (!catchError) throw error;
            this.notifyError(error as Error);
        }
    }

    /**
     * Get mode as a string (dark/light).
     */
    public theme(): "dark" | "light" | undefined { return; }

    /**
     * Restart the kernel.
     */
    public kernelRestart(): void { this.kernelSafe?.restart(); }

    /**
     * Load ressources from URL (common part to files and modules).
     */
    private async _loadFromURL(key: string, put: (arg0: string, arg1: any) => any) {
        const url = new URL(window.location.href);
        let promises = [];
        for (let fileURL of url.searchParams.getAll(key)) {
            fileURL = decodeURIComponent(fileURL);
            const filename = fileURL.split('/').pop() || "";
            promises.push(
                fetch(fileURL).then((response) => {
                    if (!response.ok) throw new Error(response.statusText);
                    return response.arrayBuffer();
                }).then((data: any) => {
                    return put(filename, data);
                }).catch((error: any) => {
                    throw new Error(`Impossible de charger le fichier ${filename} : ${error?.message ?? error.toString()}`);
                })
            );
        }
        await Promise.all(promises);
    }

    /**
     * Loading file in the (emulated) local filesystem (async).
     */
    public async putFSRessource(file: File): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = async (event) => {
                this.kernelSafe?.putRessource(file.name, reader.result as ArrayBuffer);
                this.info(`Fichier utilisable depuis ${this.kernel?.languageName()}`,
                    `${file.name} est maintenant utilisable depuis ${this.kernel?.languageName()}`);
                resolve();
            };
            reader.onerror = reject;
        });
    }

    /**
     * Load auxiliary files submited via URL (aux= parameter) (async).
     */
    public async loadURLAux() {
        if (this.kernelSafe == null) return;
        await this._loadFromURL('aux', this.kernelSafe.putFile.bind(this.kernelSafe));
    }

    /**
     * Load modules submited via URL (module= parameter) (async).
     */
    public async loadURLModules() {
        if (this.kernelSafe == null) return;
        await this._loadFromURL('module', this.kernelSafe.putModule.bind(this.kernelSafe));
    }

    /**
     * Opening file: If it has ext as extension, loading it in
     * the editor or put on (emulated) local filesystem
     * (user is asked to), otherwise, loading it in the local
     * filesystem.
     */
    protected _openFile(callbacks: { [key: string]: ((_: File) => Promise<void>) }) {
        return new Promise<void>((resolve, reject) => {
            let input = document.createElement('input');
            input.type = 'file';
            input.style.display = "none";
            input.onchange = async () => {
                if (input.files == null) return;
                for (let file of input.files) {
                    const ext = file.name.split('.').pop();
                    const callback = callbacks[ext ?? ""];
                    if (callback != null) {
                        await callback(file);
                    } else {
                        await this.putFSRessource(file);
                    }
                }
                resolve();
            };
            input.onerror = reject;

            document.body.appendChild(input);
            input.click();
            document.body.removeChild(input);
        });
    }

    /**
     * Open an URL in a new tab or download a file.
     */
    public static openURL(url: string, download?: string) {
        let anchor = document.createElement("a");
        if (download != null) anchor.download = download;
        anchor.href = url;
        anchor.target = "_blank";
        anchor.style.display = "none"; // just to be safe!
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    }


    /**
     * Returning the sharing link for the content.
     */
    public async sharingURL(key: string) {
        // to retain the Line breaks.
        let content: string = this.content().replace(/\r\n|\r|\n/g, "\r\n");
        const url = new URL(window.location.href);
        url.hash = "";
        url.searchParams.delete("from"); // take care of collapsing params
        try {
            content = await this.deflate(content);
        } catch (e) { // fallback
            content = encodeURIComponent(content).replace(/\(/g, '%28').replace(/\)/g, '%29');
        }
        url.searchParams.set(key, content);
        return url.href;
    }

    /**
     * Share content via URL.
     */
    protected async share() {
        const url = await this.sharingURL(this._urlKey);
        const message = `
Un lien permanant vers le contenu actuel a été créé.
<br>
<i class="fa fa-exclamation-circle"></i> Attention, partager un document trop long peut ne pas fonctionner avec certains navigateurs.`
        this.confirm("Partager ce document",
            message, "Copier dans le presse-papier",
            () => GUIBase.copyToClipboard(url),
            "Tester le lien", () => GUIBase.openURL(url));
    }

    /**
     * Opening file (async) and load its content.
     */
    public open(file: Blob | File) {
        return new Promise<void>((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = (event) => {
                this.setContent(event?.target?.result as string);
                resolve();
            };
            reader.onerror = reject;
        });
    }

    /**
     * Download content to file.
     */
    public download(filename?: string) {
        this.backup();
        const content = this.content().replace(/\r\n|\r|\n/g, "\r\n"); // To retain the Line breaks.
        let blob = new Blob([content], { type: "text/plain" });
        GUIBase.openURL(window.URL.createObjectURL(blob),
            filename ?? this._contentFilename);
    }

    /**
     * Copy a text to clipboard.
     */
    public static copyToClipboard = function(text: string) {
        if (navigator.clipboard != null)
            navigator.clipboard.writeText(text);
        else {
            // using the deprecated API document.execCommand('copy')
            let textArea = document.createElement("textarea");

            // Precautions from https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript

            // Place in top-left corner of screen regardless of scroll position.
            textArea.style.position = 'fixed';
            textArea.style.top = "0px";
            textArea.style.left = "0px";

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textArea.style.width = '2em';
            textArea.style.height = '2em';

            // We don't need padding, reducing the size if it does flash render.
            textArea.style.padding = "0px";

            // Clean up any borders.
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';

            // Avoid flash of white box if rendered for any reason.
            textArea.style.background = 'transparent';


            textArea.value = text;

            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                let successful = document.execCommand('copy');
                let msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            document.body.removeChild(textArea);
        }
    }

    /**
     * Compress a string to another string (URL safe).
     */
    public async deflate(content: string): Promise<string> {
        // dynamic import for webpack
        const pako = await import("pako");
        const { Base64 } = await import("js-base64");
        return Base64.fromUint8Array(pako.deflate(content), true);
    }

    /**
     * Reverse version of deflate.
     */
    public async inflate(content: string): Promise<string> {
        // dynamic import for webpack
        const pako = await import("pako");
        const { Base64 } = await import("js-base64");
        return pako.inflate(Base64.toUint8Array(content),
            { to: 'string' });
    }
}
