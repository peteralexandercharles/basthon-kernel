from . import kernel
from js import document
import io
import warnings

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


def patch_time():
    """
    Patch time.sleep to do active wait since we can't do better ATM.
    """
    import time

    def sleep(secs):
        t = time.perf_counter() + secs
        while time.perf_counter() < t:
            pass

    sleep.__doc__ = time.sleep.__doc__
    time.sleep = sleep


def patch_matplotlib():
    """
    Patch the Wasm backend of matplotlib to render figures.
    """
    from matplotlib.backends.wasm_backend import FigureCanvasWasm as wasm_backend

    # already patched?
    if hasattr(wasm_backend, "_original_show"):
        return

    import matplotlib.backends.wasm_backend as wasm_backend_module
    from matplotlib.animation import FuncAnimation
    import matplotlib as mpl

    # fix timer for animated matplotlib (self._timer not defined)
    def __init__(self, *args, **kwargs):
        self._timer = None
        wasm_backend_module.backend_bases.TimerBase.__init__(self, *args, **kwargs)
    wasm_backend_module.TimerWasm.__init__ = __init__

    # calling original draw_idle only when figure is shown
    _original_draw_idle = wasm_backend_module.FigureCanvasWasm.draw_idle
    def draw_idle(self, *args, **kwargs):
        if getattr(self, '_shown', False):
            return _original_draw_idle(self, *args, **kwargs)
    wasm_backend_module.FigureCanvasWasm.draw_idle = draw_idle

    # Allow start/stop animation
    def start(self):
        try:
            self.event_source._timer_start()
        except:
            pass
    def stop(self):
        try:
            self.event_source._timer_stop()
        except:
            pass
    def show_ani(self):
        kernel.display(self)

    FuncAnimation.start = start
    FuncAnimation.stop = stop
    FuncAnimation.show = show_ani
    FuncAnimation.display = show_ani

    # _repr_html_ for FuncAnimation redirects to to_jshtml
    mpl.rcParams['animation.html'] = 'jshtml'

    # patching root node creation
    def create_root_element(self):
        self.root = document.createElement("div")
        return self.root

    wasm_backend.create_root_element = create_root_element

    # patching element getter carefuly addressing the case where the
    # root node is not yet added to the DOM
    def get_element(self, name):
        if name == "" or not hasattr(self, "root"):
            root = document
        else:
            root = self.root
        return root.querySelector('#' + self._id + name)

    wasm_backend.get_element = get_element

    # patching show
    wasm_backend._original_show = wasm_backend.show

    def show(self):
        self._shown = True
        res = self._original_show()
        kernel.display_event({"display_type": "matplotlib",
                              "content": self.root})
        return res

    show.__doc__ = wasm_backend._original_show.__doc__
    wasm_backend.show = show


def patch_turtle():
    """
    Patch Turtle to render and download figures.
    """
    from turtle import Screen
    import turtle

    def show_scene(self):
        root = self.end_scene().cloneNode(True)
        kernel.display_event({"display_type": "turtle",
                              "content": root})
        self.restart()

    show_scene.__doc__ = Screen.show_scene.__doc__

    Screen.show_scene = show_scene

    def download(filename="turtle.svg"):
        """ Download screen as svg file. """
        return kernel.download(filename, turtle.svg())

    turtle.download = download
    turtle.__all__.append('download')


def patch_sympy():
    """
    Patch Sympy to render expression using LaTeX (and probably MathJax).
    """
    import sympy

    def pretty_print(*args, sep=' '):
        """
        Print arguments in latex form.
        """
        latex = sep.join(sympy.latex(expr) for expr in args)
        kernel.display_event({"display_type": "sympy",
                              "content": f"$${latex}$$"})

    sympy.pretty_print = pretty_print


def patch_folium():
    """
    Patch Folium to render maps.
    """
    from folium import Map

    def display(self):
        """
        Render map to html.
        """
        kernel.display(self)

    Map.display = display


def patch_pandas():
    """
    Patch Pandas to render data frames (and remove user warning about lzma).
    """
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', UserWarning)
        import pandas

    from pandas import DataFrame

    def display(self):
        """
        Render data frame to html.
        """
        kernel.display(self)

    DataFrame.display = display


def patch_PIL():
    from PIL import Image, ImageShow
    import io
    from base64 import b64encode

    # pluging for Notebook
    def _repr_png_(self):
        byio = io.BytesIO()
        self.save(byio, format='PNG')
        return b64encode(byio.getvalue()).decode()

    Image.Image._repr_png_ = _repr_png_

    # pluging image.show()
    class basthonviewer(ImageShow.Viewer):
        def show_image(self, image, **options):
            kernel.display(image)

    ImageShow._viewers = []
    ImageShow.register(basthonviewer)


def patch_scipy():
    """
    Add support for:
        * scipy.integrate.solve_ivp from the Basthon's ivp module
        * scipy.integrate.odeint (wrapper to solve_ivp)
    """
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', SyntaxWarning)
        import _ivp
        import scipy.integrate
        import numpy as np

    # add support to solve_ivp
    assert not hasattr(scipy.integrate, "solve_ivp")
    scipy.integrate.solve_ivp = _ivp.solve_ivp

    def odeint(func, y0, t, args=(),
               Dfun=None,
               tfirst=False,
               method='RK45',
               **kwargs):
        # conversion table for options names
        options = {'first_step': 'h0',
                   'min_step': 'hmin',
                   'max_step': 'hmax',
                   'rtol': 'rtol',
                   'atol': 'atol',
                   'lband': 'ml',
                   'uband': 'mu'}
        # copying options
        options = {k: kwargs.pop(v) for k, v in options.items()
                   if v in kwargs}
        # is there any that left in kwargs?
        if kwargs:
            warnings.warn(f"Parameters {','.join(kwargs.keys())} are not supported by this version of scipy.integrate.odeint.")

        dt = np.diff(t)
        if not((dt >= 0).all() or (dt <= 0).all()):
            raise ValueError("The values in t must be monotonically increasing "
                             "or monotonically decreasing; repeated values are "
                             "allowed.")

        y0 = np.atleast_1d(y0)

        fun = func
        jac = Dfun
        # support for tfirst since solve_ivp wants tfirst set to True
        if not tfirst:
            fun = lambda t, y, *a: func(y, t, *a)
            if Dfun is not None:
                jac = lambda t, y, *a: Dfun(y, t, *a)
        if Dfun is not None:
            options['jac'] = jac

        # default args
        if args == ():
            args = None

        # effective solve
        sol = scipy.integrate.solve_ivp(
            fun=fun,
            t_span=(min(t), max(t)),
            t_eval=t,
            y0=y0,
            args=args,
            method=method,
            **options)

        return sol.y.T

    scipy.integrate._odeint = scipy.integrate.odeint
    scipy.integrate.odeint = odeint
    scipy.integrate.odeint.__doc__ = scipy.integrate._odeint.__doc__


def patch_qrcode():
    """
    * Adding `_repr_svg_` and `show` to qrcode svg images.
    * Fix issue in `qrcode.image.svg.SvgPathImage._write`
    * Add shortcut format to `qrcode.make`
    * Add `download` function to `qrcode.image.base.BaseImage`
    """
    import qrcode
    import qrcode.image.base as baseimage
    import qrcode.image.svg as svg
    import qrcode.image.pil as pil

    # display svg images
    def _repr_svg_(self):
        res = io.BytesIO()
        self.save(res)
        return res.getvalue().decode('utf8')

    svg.SvgFragmentImage._repr_svg_ = _repr_svg_

    def show(self):
        """
        Display this image.
        """
        kernel.display(self)

    svg.SvgFragmentImage.show = show

    # fix qrcode.image.svg.SvgPathImage._write
    def _write(self, stream):
        flag = '_path_appended'
        if not hasattr(self, flag):
            self._img.append(self.make_path())
            setattr(self, flag, True)
        super(svg.SvgPathImage, self)._write(stream)

    svg.SvgPathImage._write = _write

    # shortcut format in qrcode.make
    qrcode._original_make = qrcode.make

    def make(*args, **kwargs):
        if 'format' in kwargs:
            format = kwargs.pop('format')
            factories = {'png': pil.PilImage,
                         'svg': svg.SvgPathImage}
            if isinstance(format, str):
                format = format.lower()
            if format not in factories:
                raise ValueError(
                    f"{format} is not supported "
                    f"(should be one of {', '.join(factories.keys())}).")
            kwargs['image_factory'] = factories[format]
        return qrcode._original_make(*args, **kwargs)

    qrcode.make = make

    # download
    def meta_download(ext=''):
        def download(self, filename=f'qrcode.{ext}'):
            """ Download image as file. """
            f = io.BytesIO()
            self.save(f)
            f.seek(0)
            # f will be closed by download
            return kernel.download(filename, f.read())
        return download

    baseimage.BaseImage.download = meta_download()
    pil.PilImage.download = meta_download('png')
    svg.SvgFragmentImage.download = meta_download('svg')


def patch_micropip():
    """ Do not load the requests package from PyPi. """
    import micropip

    old_ar = micropip.PACKAGE_MANAGER.add_requirement

    def add_requirement(requirement, ctx, transaction):
        if requirement.startswith('requests'):
            return
        return old_ar(requirement, ctx, transaction)

    micropip.PACKAGE_MANAGER.add_requirement = add_requirement


def patch_osmiter():
    """
    Remove error when loading lzma module by replacing with empty file.
    """
    from pathlib import Path

    lzma = Path('/lib/python3.8/lzma.py')
    lzma.unlink()
    with lzma.open('w'):
        pass


def patch_pyroutelib3():
    """
    Using requests.get instead of urllib.request.urlretrieve.
    """
    import pyroutelib3.datastore as ds
    import requests

    def urlretrieve(url, filename):
        response = requests.get(url)
        with open(filename, 'wb') as f:
            f.write(response.content)

    ds.urlretrieve = urlretrieve


def patch_numpy():
    """ Remove SyntaxWarning about is/== ."""
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', SyntaxWarning)
        import numpy


def patch_binarytree():
    """ Fake pkg_resources distribution to avoid not satified
    version requirement. """
    # binarytree==6.3.0 calls
    # pkg_resources.get_distribution("binarytree").version
    # at line 32 of __init__.py
    # it fails with setuptools required version 42 (we ship 40)
    # so we temporarily mock it to get the import to work
    import pkg_resources
    _tmp = pkg_resources.get_distribution
    distrib = type('dummy', (object,), {})()
    distrib.version = "6.3.0"
    # mock
    pkg_resources.get_distribution = lambda _: distrib
    import binarytree
    # unmock
    pkg_resources.get_distribution = _tmp


def patch_audioop():
    """ Fake audioop since it's not present in pyodide. """
    from types import ModuleType
    m = ModuleType("audioop")
    import sys
    sys.modules[m.__name__] = m
    m.__file__ = m.__name__ + ".py"


def patch(modules):
    """ Patch a list of modules. """
    if isinstance(modules, str):
        modules = (modules,)

    for module in modules:
        patch_func = globals().get(f"patch_{module}")
        if callable(patch_func):
            patch_func()
