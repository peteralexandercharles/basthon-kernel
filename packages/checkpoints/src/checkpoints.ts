import { Storage } from "./storage";

/**
 * A class to manage checkpoints (multiple backups).
 * Stored data is of type string (json conversion if needed).
 * One can add tags to checkpoints.
 *
 * name is a string used to identify each instance.
 * maxItems is the maximum number of checkpoints we keep before
 *          removing old ones. Set to zero to disable.
 */
export class CheckpointsManager<T> {

    private _storage: Storage<T>;
    private _tags: Storage<string[]>;
    private _maxItems: number;
    public ready: () => Promise<[void, void]>;

    /**
     * Name for the checkpoints DB.
     */
    private static _storageName(name: string) {
        return name + ".checkpoints";
    }

    /**
     * Name for the tags DB.
     */
    private static _tagsName(name: string) {
        return name + ".checkpoints-tags";
    }

    public constructor(name: string, maxItems: number = 0) {
        this._storage = new Storage<T>(
            CheckpointsManager._storageName(name));
        this._tags = new Storage<string[]>(
            CheckpointsManager._tagsName(name));
        this._maxItems = maxItems;

        this.ready = () => Promise.all([this._storage.ready(), this._tags.ready()]);
    }

    /**
     * Remove checpoints to honor maxItems-1 (use this before push).
     */
    private async _freeOneSlot() {
        while (this._maxItems > 0 && await this.length() >= this._maxItems)
            await this.removeOldest();
    };

    /**
     * Push checkpoint to DB. Timestamps are computed here.
     * tags is an array of strings.
     */
    public async push(data: T, tags: string[] | null = null) {
        if (tags == null) tags = [];
        const timestamp = Date.now().toString();
        await this._freeOneSlot();
        await this._storage.set(timestamp, data);
        await this._tags.set(timestamp, tags);
        return timestamp;
    };

    /**
     * Set a tag to a checkpoint.
     */
    public async tag(timestamp: string, tag: string) {
        const tags = (await this.getTags(timestamp)) || [];
        if (!tags.includes(tag)) {
            tags.push(tag);
            await this._tags.set(timestamp, tags);
        }
    };

    /**
     * Remove a tag from a checkpoint.
     */
    public async untag(timestamp: string, tag: string) {
        const tags = await this.getTags(timestamp) || [];
        const index = tags.indexOf(tag);
        if (index >= 0) {
            tags.splice(index, 1);
            await this._tags.set(timestamp, tags);
        }
    };

    /**
     * Add a tag to last checkpoint.
     */
    public async tagLast(tag: string) {
        const timestamp = await this.getLastTimestamp();
        if (timestamp != null) await this.tag(timestamp, tag);
    };

    /**
     * Remove a tag from last checkpoint.
     */
    public async untagLast(tag: string) {
        const timestamp = await this.getLastTimestamp();
        if (timestamp != null) await this.untag(timestamp, tag);
    };

    /**
     * Get checkpoint from its timestamp.
     */
    public async get(timestamp: string): Promise<T | null | undefined> {
        return await this._storage.get(timestamp);
    };

    /**
     * Get tags from timestamp.
     */
    public async getTags(timestamp: string): Promise<string[] | undefined> {
        const tags = await this._tags.get(timestamp);
        if (tags != null) return tags as string[];
    };

    /**
     * Does this checkpoint has this tag?
     */
    public async hasTag(timestamp: string, tag: string): Promise<boolean | null> {
        const tags = await this.getTags(timestamp);
        if (tags == null) return null;
        return tags.includes(tag);
    };

    /**
     * Get the total number of checkpoints.
     */
    public async length(): Promise<number> {
        return await this._storage.length();
    };

    /**
     * Get the array of all timestamps sorted in reverse historical order.
     */
    public async times(): Promise<string[] | undefined> {
        const keys = await this._storage.keys();
        if (keys != null)
            return keys.sort((a, b) => (parseInt(b) - parseInt(a)));
    };

    /**
     * Get timestamp at a specific index (0 is the most recent).
     */
    public async getIndexTimestamp(index: number): Promise<string | undefined> {
        const times = await this.times();
        if (times != null) return times[index];
    };

    /**
     * Get checkpoint at a specific index (0 is the most recent).
     */
    public async getIndex(index: number): Promise<T | null | undefined> {
        const timestamp = await this.getIndexTimestamp(index);
        if (timestamp != null) return await this.get(timestamp);
    };

    /**
     * Get tags at a specific index (0 is the most recent).
     */
    public async getIndexTags(index: number): Promise<string[] | undefined> {
        const timestamp = await this.getIndexTimestamp(index);
        if (timestamp != null) return await this.getTags(timestamp);
    };

    /**
     * Get the most recent timestamp. Equivalent to getIndexTimestamp(0).
     */
    public async getLastTimestamp(): Promise<string | undefined> {
        try { return await this.getIndexTimestamp(0); } catch (e) { }
    };

    /**
     * Get the most recent checkpoint. Equivalent to getIndex(0).
     */
    public async getLast(): Promise<T | null | undefined> {
        try { return await this.getIndex(0); } catch (e) { }
    };

    /**
     * Get the most recent tags. Equivalent to getIndex(0).
     */
    public async getLastTags(): Promise<string[] | undefined> {
        try { return await this.getIndexTags(0); } catch (e) { }
    };

    /**
     * Does the most recent checkpoint has this tag?
     */
    public async lastHasTag(tag: string): Promise<boolean | null> {
        const tags = await this.getLastTags();
        if (tags == null) return null;
        return tags.includes(tag);
    };

    /**
     * Remove checkpoint at a specific timestamp.
     */
    public async remove(timestamp: string) {
        await this._storage.remove(timestamp);
        await this._tags.remove(timestamp);
    };

    /**
     * Remove the oldest checkpoint.
     */
    public async removeOldest() {
        const times = await this.times();
        if (times != null && times.length > 0)
            await this.remove(times[times.length - 1]);
    };

    /**
     * Clear all checkpoints.
     */
    public async clear() {
        await this._storage.clear();
        await this._tags.clear();
    };

    /**
     * Convert a timestamp to a (french) human date : DD/MM/YYYY, hh:mm:ss
     */
    public static toHumanDate = function(timestamp: string | number) {
        if (typeof timestamp === 'string') timestamp = parseInt(timestamp);
        const date = new Date(timestamp);
        const yyyy = date.getFullYear(),
            mm = ('0' + (date.getMonth() + 1)).slice(-2),
            dd = ('0' + date.getDate()).slice(-2),
            hh = date.getHours(),
            min = ('0' + date.getMinutes()).slice(-2),
            ss = ('0' + date.getSeconds()).slice(-2);
        return (dd + '/' + mm + '/' + yyyy + ', '
            + hh + ':' + min + ':' + ss);
    };

    /**
     * Drop a checkpoint table from its name.
     */
    public static async drop(name: string) {
        await Storage.drop(CheckpointsManager._storageName(name));
        await Storage.drop(CheckpointsManager._tagsName(name));
    };
}
