import { KernelBase } from '@basthon/kernel-base';


export class KernelJavaScript extends KernelBase {
    private _context = {
        Basthon: undefined,
        Jupyter: undefined,
        window: undefined,
    };;

    constructor(rootPath: string) {
        super(rootPath);
        this._execution_count = 0;
    }

    public language() { return "javascript"; };
    public languageName() { return "Javascript"; };
    public moduleExts() { return ['js']; };

    public async launch() { };

    public eval(code: string) {
        try {
            // using context to hide several global variables
            return window.eval(`with(Basthon._context) { ${code} }`);
        } catch (e) {
            if (e instanceof SyntaxError)
                return (new Function(`with(this) { ${code} }`)).call(this._context);
            throw e;
        }
    };

    public async evalAsync(
        code: string,
        outCallback: (_: string) => void,
        errCallback: (_: string) => void,
        data: any = null): Promise<any> {
        // force interactivity in all modes
        data.interactive = true;

        // backup
        const console_log = console.log;
        const console_error = console.error;
        console.log = outCallback;
        console.error = errCallback;

        this._execution_count++;

        // evaluation
        let result: any = undefined;
        try {
            result = this.eval(code);
        } catch (e: any) {
            console.error(`Uncaught ${e.name || ""}: ${e?.message}`);
        }

        // restoration
        console.log = console_log;
        console.error = console_error;

        // return result
        if (typeof result !== 'undefined')
            result = { 'text/plain': JSON.stringify(result) };
        return [result, this._execution_count];
    };

    public ps1() { return " js> "; };

    public ps2() { return "...> "; };

    public restart() { this._execution_count = 0; };

    public more(source: string) { return false; };

    public complete(code: string): [string[], number] | [] { return []; };

    public putFile(filename: string, content: ArrayBuffer) {
        console.error(`Fichier ${filename} not added since putFile has no mean in the JS context.`);
    };

    public putModule(filename: string, content: ArrayBuffer) {
        content = new Uint8Array(content);
        const ext = filename.split('.').pop();
        switch (ext) {
            case 'js':
                let decoder = new TextDecoder("utf-8");
                const _content = decoder.decode(content);
                this.eval(_content);
                break;
            default:
                throw { message: "Only '.js' files supported." };
        }
    };
}
