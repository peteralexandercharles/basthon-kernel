\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{documentation}[2020/11/01 v1.0 custom article class]

% option processing
\newif\ifpython\pythonfalse
\DeclareOption{python}{
  \pythontrue
}
\newif\ifnosexy\nosexyfalse
\DeclareOption{nosexy}{
  \nosexytrue
}
\ProcessOptions\relax

\LoadClass[a4paper, 11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}

\usepackage[french]{babel}

\usepackage{amssymb}
\usepackage{multicol}

\usepackage{graphicx}
\usepackage{tikz}

\definecolor{bluepython}{HTML}{366f9e} % python blue
\definecolor{yellowpython}{HTML}{ffc938} % python yellow

\usepackage[
hidelinks,
colorlinks=true,
%urlcolor=bluepython,
allcolors=bluepython]{hyperref}

\usepackage{array}[=2016-10-06]
\newcolumntype{M}[1]{>{\centering\arraybackslash} m{#1}}
\newcolumntype{P}[1]{>{\arraybackslash} m{#1}}
\newcolumntype{N}{@{}m{0pt}@{}}

\usepackage{fontawesome5}

\usepackage[ddmmyyyy]{datetime}
\renewcommand{\dateseparator}{/}

% customized enumerate
\usepackage[inline]{enumitem}
\setenumerate[1]{label=\arabic*)}
\setenumerate[2]{label=\alph*)}
\setenumerate[3]{label=\roman*)}
% bullet in itemize
\frenchbsetup{StandardLists=true}
\setlist[enumerate,itemize]{topsep=0pt,itemsep=0pt}

\usepackage[framemethod=TikZ]{mdframed}

\usepackage[margin=1.5cm]{geometry}

% default to sans serif font
\renewcommand{\familydefault}{\sfdefault}

\usepackage[
    type={CC},
    modifier={by-nc-sa},
    version={4.0},
    ]{doclicense}
    
\tikzset{console-button/.style={white, draw, rounded corners,
    rectangle, fill=bluepython, outer sep=0pt, inner sep=0pt,
    minimum height=30pt, minimum width=42pt}}
%
\newcommand{\buttonconsole}[1]{ 
  \tikz{\node[console-button]
    {\large\faIcon{#1}};}
}
\newcommand{\textbuttonconsole}[2]{ 
  \tikz{\node[console-button]
    {\hspace*{14pt}{\large\faIcon{#1}}{\hspace*{7pt}{#2}}\hspace*{14pt}};}
}
% 
\tikzset{notebook-button/.style={black, draw, rounded corners=2pt,
    rectangle, outer sep=0pt, inner sep=0pt,
    minimum height=30pt, minimum width=30pt}}
%
\newcommand{\buttonnotebook}[2][regular]{ 
  \tikz{\node[notebook-button]
    {\Large\faIcon[#1]{#2}};}
}


% hilighted python code
\ifpython
  \usepackage{minted}
  \usepackage{tcolorbox}
  \usepackage{fancyvrb}
  
  \newminted[pythoncmdcode]{pycon}{mathescape, autogobble}
  \newminted[pythoncode]{python}{mathescape, autogobble}
  \newminted[jupytercode]{python}{mathescape, autogobble}
  \BeforeBeginEnvironment{jupytercode}{\begin{tcolorbox}[boxrule=0.65pt,
      colback=black!2]}
    \AfterEndEnvironment{jupytercode}{\end{tcolorbox}}%

  \newcommand{\inputjupytercode}[2][]{%
    \begin{tcolorbox}[boxrule=0.65pt, colback=black!2]
      \inputminted[autogobble,#1]{python}{#2}
    \end{tcolorbox}%
  }
  
  \newminted[jupyterres]{pycon}{mathescape, autogobble,xleftmargin=\parindent}
  \BeforeBeginEnvironment{jupyterres}{\endgraf\setlength\partopsep{-\topsep}}%
    \AfterEndEnvironment{jupyterres}{\vspace{.5\baselineskip}}%
  %\newenvironment{jupyterres}%
  %{\endgraf\setlength\partopsep{-\topsep}\Verbatim}%
  %{\endVerbatim\vspace{.5\baselineskip}}

  \newmintinline[pythoninline]{python}{}
  \newmintinline[pil]{python}{}
  \newmintinline[pyconinline]{pycon}{}
      
\fi

% indented minipage
\newenvironment{minipageindent}[1]{%
  \edef\myindent{\the\parindent}%
  \begin{minipage}{#1}%
    \setlength{\parindent}{\myindent}}{\end{minipage}}

% colors
\definecolor{colortheme1}{rgb}{0.23,0.4,0.7}
\definecolor{colortheme2}{rgb}{0.7,0.2,0.2}
\definecolor{colortheme3}{rgb}{0.392, 0.667, 0.231}
\definecolor{colortheme4}{rgb}{0.627, 0.231, 0.651}
\definecolor{colortheme5}{rgb}{0, 0.643, 0.996}
\definecolor{colortheme6}{RGB}{255, 216, 71}
\definecolor{colortheme7}{RGB}{160, 160, 160}
\definecolor{colortheme8}{RGB}{242, 127, 13}
\definecolor{colortheme9}{rgb}{0.906, 0.494, 0.137}

\extractcolorspec{colortheme1}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{colortheme}{rgb}{\colorthemespec}

\extractcolorspec{colortheme2}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{data}{rgb}{\colorthemespec}

\extractcolorspec{colortheme1}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{algo}{rgb}{\colorthemespec}

\extractcolorspec{colortheme9}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{langages}{rgb}{\colorthemespec}

\extractcolorspec{colortheme3}{\colorthemespec}
\expandafter\convertcolorspec\colorthemespec{rgb}\colorthemespec
\definecolor{machines}{rgb}{\colorthemespec}

\usepackage[os=win]{menukeys}
\renewmenumacro{\keys}[+]{shadowedroundedkeys}


\usepackage[vlined, french, onelanguage]{algorithm2e}
\DontPrintSemicolon

\usepackage{fancyvrb}
\DefineVerbatimEnvironment{shell}{Verbatim}{xleftmargin=2em}

\usepackage{tcolorbox}
\tcbuselibrary{skins, breakable}
\usepackage{fontawesome5}

\ifnosexy
  \theoremstyle{definition}
  \newtheorem{exercice}[equation]{Exercice}
  \newtheorem*{exercice*}{Exercice}
\else

\newtcolorbox{pedaboxbase}[4]{%
  enhanced,
  breakable,
  enlarge left by=10mm,
  width=\linewidth-10mm,
  sharp corners,
  rightrule=0pt, bottomrule=0pt, leftrule=1mm,
  colback=#1!5, colframe=#1,
  #4,
  overlay unbroken and first ={
    \node[#1, outer sep=0pt, inner sep=0pt, anchor=north east, minimum
    width=1.7cm] (icon) at (frame.north west)
    {#2};
    \node[below, outer sep=15pt, #1] at (icon) {\sffamily\footnotesize #3};
  }}

\newenvironment{titledpedabox}[5]{%
  % 
  \refstepcounter{#5}
  % 
  \begin{pedaboxbase}{#1}{#2}{#3}{rounded corners=northwest,
  arc=1.25ex, toprule=1mm, title=#3 \arabic{#5}\ {#4}, coltitle=#1,
  attach boxed title to top left={yshift=-3mm,xshift=5mm, yshifttext=-1mm},
  boxed title style={
    boxrule=0pt,
    colframe=white,
  },
  fonttitle=\sffamily\bfseries,
  colbacktitle=white}
% 
}{\end{pedaboxbase}}

\newtcolorbox{titledroundpedabox}[2]{%
  enhanced,
  breakable,
  rounded corners,
  toprule=1mm, rightrule=1mm, bottomrule=1mm, leftrule=1mm,
  colback=#1!5, colframe=#1,
  coltitle=#1,
  attach boxed title to top left={yshift=-3mm,xshift=5mm, yshifttext=-1mm},
  title=#2,
  boxed title style={
    boxrule=0pt,
    colframe=white,
  },
  fonttitle=\sffamily\bfseries,
  colbacktitle=white,
}

\newtcolorbox{leftpedabox}[3]{
  enhanced,
  boxsep=3pt,
  arc=1.25ex,
  colback=white,
  colframe=#2,
  boxrule=1mm,
  leftrule=18pt,
  coltitle=#2,
  attach boxed title to top left={yshift=-3mm,xshift=15mm, yshifttext=-1mm},
  title=#1,
  boxed title style={
    boxrule=0pt,
    colframe=white,
  },
  fonttitle=\sffamily\bfseries,
  colbacktitle=white,
  overlay unbroken and first ={%
    \node[rotate=90,
    minimum width=1cm,
    anchor=south,
    font=\Large\sffamily\bfseries,
    yshift=-18pt,
    white]
    at (frame.west) {#3};
  }}

\newenvironment{pedabox}[3]{%
  \begin{pedaboxbase}{#1}{#2}{#3}{toprule=0pt}
}{\end{pedaboxbase}}

\newenvironment{pypoint}{\begin{pedabox}{colortheme3}{\includegraphics[width=1.3cm]{python-logo-tall}}{}}{\end{pedabox}}
\newenvironment{attention}{\begin{pedabox}{colortheme7}{\Huge\faIcon{exclamation-triangle}}{Attention}}{\end{pedabox}}
\newenvironment{rappels}{\begin{pedabox}{colortheme4}{\Huge\faIcon{sync-alt}}{Rappels}}{\end{pedabox}}
\newenvironment{astuce}{\begin{pedabox}{colortheme6}{\Huge\faIcon[regular]{lightbulb}}{Astuce}}{\end{pedabox}}
\newenvironment{info}{\begin{pedabox}{colortheme}{\Huge\faIcon{info-circle}}{Info}}{\end{pedabox}}
\newenvironment{exemple}{\begin{pedabox}{colortheme8}{\Huge\faIcon[regular]{life-ring}}{Exemple}}{\end{pedabox}}
\fi

\endinput